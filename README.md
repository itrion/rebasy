## Record Backend System REBASY 
To use Record Backend System you need to be a registered admin. In order to authenticate yourself please visit the link below. The application will ask you permission for getting your email.

    https://rocky-caverns-9766.herokuapp.com/login

### /api/1.0
Use the base this base url _https://rocky-caverns-9766.herokuapp.com_ and append the _/api/version_. For example to get list of customer IDs:

    https://rocky-caverns-9766.herokuapp.com/api/1.0/customer


#### Register an admin
    POST /admin
###### Parameters
* access_token
* admin_name
###### Result
* {"admin_name":"value"}
* {"error":"AdminAlreadyExist value"}
* {"error":"MissingParameter parameter"}

#### Delete a registered admin
    DELETE /admin
###### Parameters
* access_token
* admin_name
###### Result
* {"admin_name":"value"}
* {"error":"MissingParameter parameter"}

#### Create customer
    POST /customer
###### Parameters
* access_token
* customer_name
* customer_lastname
* customer_id
###### Body
* customer photo in base64 encoding
###### Result
* {"customer_id":"id"}
* {"error":"CustomerAlreadyExist customer_id"}
* {"error":"MissingParameter parameter"}
* {"error":"EmptyBody check API documentation"}

#### Retrieve all customers
    GET /customer
###### Parameters
* access_token
###### Result
* {"customer_ids":["ID1",...,"IDN"]}

#### Retrieve specific customer
    GET /customer/:id
###### Parameters
* access_token
###### Result
* {"name":"value","last_name":"value","id":"value","photo":{"base64":"value"}}
* {"error":"CustomerNotFound id"}

#### Delete customer
    DELETE /customer/:id
###### Parameters
* access_token
###### Result
* {"customer_id":"id"}

#### Retrieve orders of customer
    GET /order/customer/:customer_id
###### Parameters
* access_token
###### Result
* [{"id":"value1}..{"id":"valueN}]
* [] when no orders for customer

#### Retrieve order information
    GET /order/:order_id
###### Parameters
* access_token
###### Result
* {"date":"orderdate"
    "customer_name":"value",
    "customer_lastname":"value",
    "customer_id":"value",
    "product_name":"value",
    "product_description":"value",
    "product_id":"value"}

#### Create order for customer
    POST /order
###### Parameters
* access_token
* date
* customer_id
* product_id
###### Result
* {"order_id":"value"}
* {"error":"CustomerNotFound customer_id"}
* {"error":"ProductNotFound product_id"}
* {"error":"MissingParameter parameter"}


### Under construction
#### Update customer
    PUT /customer/:id
###### Parameters
* access_token
* name
* last_name
* b64_photo
###### Result
* Updated customer_id
