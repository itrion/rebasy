package rebasy.usecases.orders;

import rebasy.model.customers.CustomerID;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;

import java.util.List;

public class ListOrdersMadeByCustomer {
	private final OrderService orderService;

	public ListOrdersMadeByCustomer(OrderService orderService) {
		this.orderService = orderService;
	}

	public List<OrderID> execute(CustomerID customerId) {
		return orderService.getOrdersBy(customerId);
	}
}
