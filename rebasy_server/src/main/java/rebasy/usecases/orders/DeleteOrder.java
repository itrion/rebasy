package rebasy.usecases.orders;

import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;

public class DeleteOrder {
	private final OrderService orderService;

	public DeleteOrder(OrderService orderService) {
		this.orderService = orderService;
	}

	public void execute(OrderID orderId) {
		orderService.deleteOrder(orderId);
	}
}
