package rebasy.usecases.orders;

import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService.CustomerNotFound;
import rebasy.model.orders.Date;
import rebasy.model.orders.Order;
import rebasy.model.orders.OrderService;
import rebasy.model.orders.ProductID;
import rebasy.model.orders.ProductService.ProductNotFound;

public class PlaceOrder {
	private final OrderService orderService;

	public PlaceOrder(OrderService orderService) {
		this.orderService = orderService;
	}

	public Order execute(Date date, CustomerID customerId, ProductID productId) throws CustomerNotFound, ProductNotFound {
		return orderService.createOrder(date, customerId, productId);
	}
}
