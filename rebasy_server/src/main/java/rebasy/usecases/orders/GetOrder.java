package rebasy.usecases.orders;

import rebasy.model.customers.CustomerService;
import rebasy.model.orders.Order;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;
import rebasy.model.orders.ProductService;

import static rebasy.model.orders.OrderService.OrderNotFound;

public class GetOrder {
	private final OrderService orderService;

	public GetOrder(OrderService orderService) {
		this.orderService = orderService;
	}

	public Order execute(OrderID orderId) throws OrderNotFound, ProductService.ProductNotFound, CustomerService.CustomerNotFound {
		return orderService.getOrder(orderId);
	}
}
