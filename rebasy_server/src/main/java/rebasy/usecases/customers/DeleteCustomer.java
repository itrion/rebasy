package rebasy.usecases.customers;

import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;

public class DeleteCustomer {
	private final CustomerService customerService;

	public DeleteCustomer(CustomerService customerService) {
		this.customerService = customerService;
	}

	public void execute(CustomerID id) {
		customerService.delete(id);
	}
}
