package rebasy.usecases.customers;

import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;
import rebasy.model.customers.Photo;

public class AddCustomer {
	private final CustomerService customerService;

	public AddCustomer(CustomerService customerService) {
		this.customerService = customerService;
	}

	public Customer execute(String name, String lastName, DNI dni, Photo photo) throws CustomerService.CustomerAlreadyExistException {
		return customerService.addCustomer(name, lastName, dni, photo);
	}
}
