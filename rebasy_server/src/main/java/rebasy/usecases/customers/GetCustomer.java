package rebasy.usecases.customers;

import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.CustomerService.CustomerNotFound;

public class GetCustomer {
	private final CustomerService customerService;

	public GetCustomer(CustomerService customerService) {
		this.customerService = customerService;
	}

	public Customer execute(CustomerID id) throws CustomerNotFound {
		return customerService.getCustomerWith(id);
	}
}
