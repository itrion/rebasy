package rebasy.usecases.customers;

import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;

import java.util.List;

public class GetCustomers {
	private final CustomerService customerService;

	public GetCustomers(CustomerService customerService) {
		this.customerService = customerService;
	}

	public List<CustomerID> execute() {
		return customerService.getCustomersIds();
	}
}
