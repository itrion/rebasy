package rebasy.usecases.admins;

import rebasy.model.admins.AdminService;
import rebasy.model.admins.AdminService.AdminAlreadyExist;

public class RegisterAdminUser {
	private final AdminService adminService;

	public RegisterAdminUser(AdminService adminRepository) {
		this.adminService = adminRepository;
	}

	public void execute(String userName) throws AdminAlreadyExist {
		adminService.createNewAdmin(userName);
	}
}
