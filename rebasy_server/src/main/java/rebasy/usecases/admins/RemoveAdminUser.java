package rebasy.usecases.admins;

import rebasy.model.admins.AdminService;

public class RemoveAdminUser {
	private final AdminService adminService;

	public RemoveAdminUser(AdminService adminService) {
		this.adminService = adminService;
	}

	public void execute(String adminName) {
		adminService.deleteAdmin(adminName);
	}
}
