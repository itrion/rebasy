package rebasy.infrastructure;

import rebasy.infrastructure.auth.AuthService;
import rebasy.infrastructure.auth.GoogleAuthService;
import rebasy.infrastructure.repositories.InMemoryAdminRepository;
import rebasy.infrastructure.repositories.InMemoryCustomerRepository;
import rebasy.infrastructure.repositories.InMemoryOrderRepository;
import rebasy.infrastructure.repositories.InMemoryProductRepository;
import rebasy.model.admins.AdminService;
import rebasy.model.customers.CustomerService;
import rebasy.model.orders.Date;
import rebasy.model.orders.OrderService;
import rebasy.model.orders.ProductService;

import java.net.MalformedURLException;
import java.util.Optional;

import static java.lang.Integer.parseInt;

public class Main {

	public static final String REBASY_CLIENT_ID = getFromEnvironment("REBASY_CLIENT_ID");
	public static final String REBASY_CLIENT_SECRET = getFromEnvironment("REBASY_CLIENT_SECRET");
	public static final String HOST = getFromEnvironment("REBASY_DOMAIN");
	public static final int PORT = parseInt(getFromEnvironment("PORT"));


	public static void main(String[] args) throws MalformedURLException, ProductService.ProductNotFound, CustomerService.CustomerNotFound {
		AdminService adminService = getAdminService();
		AuthService authService = getAuthService(adminService);
		CustomerService customerService = getCustomerService();
		ProductService productService = getProductService();
		OrderService orderService = getOrderService(customerService, productService);

		RebasyServer server = new RebasyServer(adminService,
				customerService,
				orderService,
				authService,
				PORT);

		server.start();
	}

	private static AdminService getAdminService() {
		return new AdminService(new InMemoryAdminRepository());
	}

	private static CustomerService getCustomerService() {
		return new CustomerService(new InMemoryCustomerRepository());
	}

	private static ProductService getProductService() {
		return new ProductService(new InMemoryProductRepository());
	}

	private static OrderService getOrderService(CustomerService customerService, ProductService productService) throws ProductService.ProductNotFound, CustomerService.CustomerNotFound {
		OrderService orderService = new OrderService(new InMemoryOrderRepository(), customerService, productService);
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(0), productService.getProducts().get(0).id());
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(1), productService.getProducts().get(1).id());
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(1), productService.getProducts().get(1).id());
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(2), productService.getProducts().get(2).id());
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(2), productService.getProducts().get(2).id());
		orderService.createOrder(new Date(""), customerService.getCustomersIds().get(2), productService.getProducts().get(2).id());
		return orderService;
	}

	private static AuthService getAuthService(AdminService adminService) throws MalformedURLException {
		return new GoogleAuthService(REBASY_CLIENT_ID,
				REBASY_CLIENT_SECRET,
				HOST,
				PORT,
				adminService);
	}

	private static String getFromEnvironment(String variableName) {
		return Optional.ofNullable(System.getenv(variableName))
				.orElseThrow(() -> new RuntimeException("Ensure you have defined " + variableName + " in your environment variables"));
	}
}
