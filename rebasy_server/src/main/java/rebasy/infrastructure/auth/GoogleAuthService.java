package rebasy.infrastructure.auth;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import rebasy.infrastructure.serializer.google.PersonResponseParser;
import rebasy.model.admins.AdminService;

import java.net.MalformedURLException;
import java.util.*;

public class GoogleAuthService implements AuthService {
	private static final String GOOGLE_AUTH_2_0_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth";
	private static final String GOOGLE_USER_INFO_EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
	private static final String GOOGLE_PEOPLE_GET_ME_ENDPOINT = "https://www.googleapis.com/plus/v1/people/me";


	private final Map<String, String> loggedUsers;
	private final String clientId;
	private final String clientSecret;
	private final AdminService adminService;
	private final List<String> states;
	private final String callbackUrl;
	private final OAuthClient oAuthClient;


	public GoogleAuthService(String clientId, String clientSecret, String host, int port, AdminService adminService) throws MalformedURLException {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.adminService = adminService;
		this.loggedUsers = new HashMap<>();
		this.states = new ArrayList<>();
		this.callbackUrl = scheme(host) + host + port(host, port) + "/oauth2callback";
		this.oAuthClient = new OAuthClient(new URLConnectionClient());
	}

	private String scheme(String host) {
		return isLocalhost(host) ? "http://" : "https://";
	}

	private String port(String host, int port) {
		return isLocalhost(host) ? ":" + port : "";
	}

	private boolean isLocalhost(String host) {
		return "localhost".equals(host);
	}

	@Override
	public boolean isLogged(String token) {
		String user = loggedUsers.get(token);
		return user != null && adminService.exist(user);
	}

	@Override
	public String authRequestUrl() {
		try {
			return OAuthClientRequest
					.authorizationLocation(GOOGLE_AUTH_2_0_ENDPOINT)
					.setClientId(clientId)
					.setParameter("access_type", "offline")
					.setParameter("state", random())
					.setParameter("response_type", "code")
					.setScope(GOOGLE_USER_INFO_EMAIL_SCOPE)
					.setRedirectURI(callbackUrl)
					.buildQueryMessage()
					.getLocationUri();
		} catch (OAuthSystemException e) {
			throw new RuntimeException(e);
		}
	}

	private String random() {
		String result = "" + new Random(System.currentTimeMillis()).nextInt();
		states.add(result);
		return result;
	}

	@Override
	public boolean isAValidState(String state) {
		return states.contains(state);
	}

	@Override
	public String grantAccess(String code) throws InvalidUser {
		try {
			states.remove(code);

			String accessToken = exchangeAccessTokenFrom(code);

			String userEmail = getAccountEmail(accessToken);
			if (!adminService.exist(userEmail)) throw new InvalidUser(userEmail);
			loggedUsers.put(accessToken, userEmail);
			return accessToken;

		} catch (OAuthSystemException | OAuthProblemException e) {
			throw new RuntimeException(e);
		}
	}

	private String exchangeAccessTokenFrom(String code) throws OAuthSystemException, OAuthProblemException {
		return oAuthClient.accessToken(exchangeRequest(code), OAuthJSONAccessTokenResponse.class)
				.getAccessToken();
	}

	private String getAccountEmail(String accessToken) throws OAuthSystemException, OAuthProblemException {
		return new PersonResponseParser(resource(peopleGetMe(accessToken))).email();
	}

	private OAuthClientRequest peopleGetMe(String accessToken) throws OAuthSystemException {
		return new OAuthBearerClientRequest(GOOGLE_PEOPLE_GET_ME_ENDPOINT)
				.setAccessToken(accessToken)
				.buildQueryMessage();
	}

	private String resource(OAuthClientRequest clientRequest) throws OAuthSystemException, OAuthProblemException {
		return oAuthClient.resource(clientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class).getBody();
	}

	private OAuthClientRequest exchangeRequest(String code) throws OAuthSystemException {
		return OAuthClientRequest
				.tokenLocation("https://www.googleapis.com/oauth2/v4/token")
				.setGrantType(GrantType.AUTHORIZATION_CODE)
				.setClientId(clientId)
				.setClientSecret(clientSecret)
				.setRedirectURI(callbackUrl)
				.setCode(code)
				.buildQueryMessage();
	}
}
