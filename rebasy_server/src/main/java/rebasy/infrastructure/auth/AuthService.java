package rebasy.infrastructure.auth;

public interface AuthService {
	boolean isLogged(String token);

	String authRequestUrl();

	boolean isAValidState(String state);

	String grantAccess(String code) throws InvalidUser;

	class InvalidUser extends Exception {
		public InvalidUser(String userEmail) {
			super(userEmail);
		}
	}

}
