package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.admins.AdminService;
import rebasy.usecases.admins.RemoveAdminUser;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.getParam;

public class DeleteAdmin implements Route {
	private final RemoveAdminUser usecase;
	private final Serializer serializer;

	public DeleteAdmin(AdminService adminService, Serializer serializer) {
		this.serializer = serializer;
		this.usecase = new RemoveAdminUser(adminService);
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		String name = getParam(request, Parameters.ADMIN_NAME);
		usecase.execute(name);
		return serializer.keyValue(Parameters.ADMIN_NAME, name);
	}
}
