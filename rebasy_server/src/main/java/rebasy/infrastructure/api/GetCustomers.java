package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;
import spark.Request;
import spark.Response;
import spark.Route;

import static java.util.stream.Collectors.toList;

public class GetCustomers implements Route {
	private final rebasy.usecases.customers.GetCustomers usecase;
	private final Serializer serializer;

	public GetCustomers(CustomerService customerService, Serializer serializer) {
		this.usecase = new rebasy.usecases.customers.GetCustomers(customerService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		return serializer.keyValue("customer_ids",
				usecase.execute().stream()
						.map(CustomerID::value)
						.collect(toList()));
	}
}
