package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.orders.Order;
import rebasy.model.orders.OrderService;
import spark.Request;
import spark.Response;
import spark.Route;

public class GetOrder implements Route {
	private final rebasy.usecases.orders.GetOrder usecase;
	private final Serializer serializer;

	public GetOrder(OrderService orderService, Serializer serializer) {
		this.usecase = new rebasy.usecases.orders.GetOrder(orderService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		Order order = usecase.execute(Helpers.orderID(request));
		return serializer.value(new OrderView(order));
	}

	private class OrderView {

		private final String date;
		private final String customer_name;
		private final String customer_lastname;
		private final String customer_id;
		private final String product_name;
		private final String product_description;
		private final String product_id;

		public OrderView(Order order) {
			date = order.date().toString();
			customer_name = order.customer().name();
			customer_lastname = order.customer().lastName();
			customer_id = order.customer().id().value();
			product_name = order.product().name();
			product_description = order.product().description();
			product_id = order.product().id().value();
		}
	}
}
