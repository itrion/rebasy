package rebasy.infrastructure.api;

import rebasy.model.customers.CustomerID;
import rebasy.model.orders.Date;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.ProductID;
import spark.Request;

public class Helpers {
	public static String getParam(Request request, String param) throws MissingParameterException {
		String value;
		if ((value = request.queryParams(param)) != null) return value;
		if ((value = request.params(param)) != null) return value;
		throw new MissingParameterException(param);
	}

	public static String getBody(Request request) throws EmptyBody {
		String body = request.body();
		if (body != null && !body.isEmpty()) return body;
		throw new EmptyBody("check API documentation");
	}

	public static OrderID orderID(Request request) throws MissingParameterException {
		return new OrderID(getParam(request, Parameters.ORDER_ID));
	}

	public static ProductID productId(Request request) throws MissingParameterException {
		return new ProductID(getParam(request, Parameters.PRODUCT_ID));
	}

	public static Date date(Request request) throws MissingParameterException {
		return new Date(getParam(request, Parameters.DATE));
	}

	public static String accessToken(Request request) throws MissingParameterException {
		return getParam(request, Parameters.ACCESS_TOKEN);
	}

	public static String oauthProviderCode(Request request) throws MissingParameterException {
		return getParam(request, Parameters.OAUTH_PROVIDER_CODE);
	}

	public static String oauthProviderState(Request request) throws MissingParameterException {
		return getParam(request, Parameters.OAUTH_PROVIDER_STATE);
	}

	public static class MissingParameterException extends Exception {
		public MissingParameterException(String parameter) {
			super(parameter + " parameter is mandatory");
		}
	}

	public static class EmptyBody extends Exception {
		public EmptyBody(String message) {
			super(message);
		}
	}

	public static String customerName(Request request) throws MissingParameterException {
		return getParam(request, Parameters.CUSTOMER_NAME);
	}

	public static String customerLastName(Request request) throws MissingParameterException {
		return getParam(request, Parameters.CUSTOMER_LAST_NAME);
	}

	public static CustomerID customerId(Request request) throws MissingParameterException {
		return new CustomerID(getParam(request, Parameters.CUSTOMER_ID));
	}

	public static String customerPhoto(Request request) throws EmptyBody {
		return getBody(request);
	}

}
