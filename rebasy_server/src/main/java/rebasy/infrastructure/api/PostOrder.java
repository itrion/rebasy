package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.orders.Order;
import rebasy.model.orders.OrderService;
import rebasy.usecases.orders.PlaceOrder;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.*;

public class PostOrder implements Route {
	private final PlaceOrder usecase;
	private final Serializer serializer;

	public PostOrder(OrderService orderService, Serializer serializer) {
		this.usecase = new PlaceOrder(orderService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		Order order = usecase.execute(date(request),
				customerId(request),
				productId(request));
		return serializer.keyValue(Parameters.ORDER_ID, order.id().value());
	}
}
