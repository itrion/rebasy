package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.customerId;

public class DeleteCustomer implements Route {
	private final rebasy.usecases.customers.DeleteCustomer usecase;
	private final Serializer serializer;

	public DeleteCustomer(CustomerService customerService, Serializer serializer) {
		this.usecase = new rebasy.usecases.customers.DeleteCustomer(customerService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		usecase.execute(customerId(request));
		return serializer.keyValue(Parameters.CUSTOMER_ID, customerId(request));
	}
}
