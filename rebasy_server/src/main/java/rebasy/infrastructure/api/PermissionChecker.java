package rebasy.infrastructure.api;

import rebasy.infrastructure.auth.AuthService;
import spark.Filter;
import spark.Request;
import spark.Response;

import static rebasy.infrastructure.api.Helpers.accessToken;

public class PermissionChecker implements Filter {
	private final AuthService authService;

	public PermissionChecker(AuthService authService) {
		this.authService = authService;
	}

	@Override
	public void handle(Request request, Response response) throws Exception {
		try {
			String accessToken = accessToken(request);
			if (!authService.isLogged(accessToken)) throw new PermissionDenied("invalid token");
		} catch (Helpers.MissingParameterException e) {
			throw new PermissionDenied("first get an access token");
		}
	}

	public class PermissionDenied extends Exception {
		public PermissionDenied(String message) {
			super(message);
		}
	}
}
