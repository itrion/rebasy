package rebasy.infrastructure.api;

import rebasy.infrastructure.auth.AuthService;
import rebasy.infrastructure.serializer.Serializer;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.accessToken;

public class Login implements Route {

	private final AuthService authService;
	private final Serializer serializer;

	public Login(AuthService authService, Serializer serializer) {
		this.authService = authService;
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		try {
			String accessToken = accessToken(request);
			if (authService.isLogged(accessToken))
				response.redirect("/");
			return serializer.error("InvalidToken");
		} catch (Helpers.MissingParameterException e) {
			response.redirect(authService.authRequestUrl());
		}
		return "";
	}
}
