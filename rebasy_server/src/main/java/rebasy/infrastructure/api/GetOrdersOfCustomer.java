package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;
import rebasy.usecases.orders.ListOrdersMadeByCustomer;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.List;

import static rebasy.infrastructure.api.Helpers.customerId;

public class GetOrdersOfCustomer implements Route {
	private final Serializer serializer;
	private final ListOrdersMadeByCustomer usecase;

	public GetOrdersOfCustomer(OrderService orderService, Serializer serializer) {
		this.usecase = new ListOrdersMadeByCustomer(orderService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		List<OrderID> orders = usecase.execute(customerId(request));
		return serializer.value(orders);
	}
}
