package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.Photo;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.customerId;

public class GetCustomer implements Route {
	private final Serializer serializer;
	private final rebasy.usecases.customers.GetCustomer getCustomer;

	public GetCustomer(CustomerService customerService, Serializer serializer) {
		this.getCustomer = new rebasy.usecases.customers.GetCustomer(customerService);
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		try {
			Customer customer = getCustomer.execute(customerId(request));
			return serializer.value(new CustomerView(customer));
		} catch (Exception e) {
			return serializer.error(e.getClass().getSimpleName() + " " + e.getMessage());
		}
	}

	private class CustomerView {
		private final String name;
		private final String last_name;
		private final String id;
		private final Photo photo;

		public CustomerView(Customer customer) {
			this.name = customer.name();
			this.last_name = customer.lastName();
			this.id = customer.id().value();
			this.photo = customer.photo();
		}
	}
}
