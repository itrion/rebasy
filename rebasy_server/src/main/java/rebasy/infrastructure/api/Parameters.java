package rebasy.infrastructure.api;

public interface Parameters {
	String ADMIN_NAME = "admin_name";
	String CUSTOMER_NAME = "customer_name";
	String CUSTOMER_LAST_NAME = "customer_lastname";
	String CUSTOMER_ID = "customer_id";
	String ORDER_ID = "order_id";
	String PRODUCT_ID = "product_id";
	String DATE = "date";
	String ACCESS_TOKEN = "access_token";
	String OAUTH_PROVIDER_CODE = "code";
	String OAUTH_PROVIDER_STATE = "state";
}
