package rebasy.infrastructure.api;

import org.eclipse.jetty.http.HttpStatus;
import rebasy.infrastructure.auth.AuthService;
import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.admins.AdminService;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.oauthProviderCode;
import static rebasy.infrastructure.api.Helpers.oauthProviderState;

public class OAuth2Callback implements Route {
	private final AuthService authService;
	private final AdminService adminService;
	private final Serializer serializer;

	public OAuth2Callback(AuthService authService, AdminService adminService, Serializer serializer) {
		this.authService = authService;
		this.adminService = adminService;
		this.serializer = serializer;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		try {
			String code = oauthProviderCode(request);
			String state = oauthProviderState(request);
			if (!authService.isAValidState(state)) return serializer.error("invalid response from oauth provide");
			String accessToken = authService.grantAccess(code);
			return serializer.keyValue("key", accessToken);
		} catch (Helpers.MissingParameterException e) {
			response.status(HttpStatus.UNAUTHORIZED_401);
			return serializer.error("login aborted by user");
		}
	}
}
