package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;
import rebasy.model.customers.Photo;
import rebasy.usecases.customers.AddCustomer;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.*;

public class PostCustomer implements Route {
	private final AddCustomer usecase;
	private final Serializer serializer;

	public PostCustomer(CustomerService customerService, Serializer serializer) {
		this.serializer = serializer;
		this.usecase = new AddCustomer(customerService);
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		Customer customer = usecase.execute(customerName(request),
				customerLastName(request),
				new DNI(customerId(request).value()),
				new Photo(customerPhoto(request)));
		return serializer.keyValue(Parameters.CUSTOMER_ID, customer.id().value());
	}
}