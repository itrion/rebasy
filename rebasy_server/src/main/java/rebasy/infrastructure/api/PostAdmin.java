package rebasy.infrastructure.api;

import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.admins.AdminService;
import rebasy.usecases.admins.RegisterAdminUser;
import spark.Request;
import spark.Response;
import spark.Route;

import static rebasy.infrastructure.api.Helpers.getParam;

public class PostAdmin implements Route {
	private final RegisterAdminUser usecase;
	private final Serializer serializer;

	public PostAdmin(AdminService adminRepository, Serializer serializer) {
		this.serializer = serializer;
		this.usecase = new RegisterAdminUser(adminRepository);
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		String adminName = getParam(request, Parameters.ADMIN_NAME);
		usecase.execute(adminName);
		return serializer.keyValue(Parameters.ADMIN_NAME, adminName);
	}
}
