package rebasy.infrastructure.serializer.google;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PersonResponseParser {
	private final JsonObject person;

	public PersonResponseParser(String response) {
		this.person = new Gson().fromJson(response, JsonObject.class);
	}

	public String email() {
		JsonArray array = person.get("emails")
				.getAsJsonArray();
		for (JsonElement jsonElement : array) {
			JsonObject email = jsonElement.getAsJsonObject();
			if ("account".equals(email.get("type").getAsString())) {
				return email.get("value").getAsString();
			}
		}
		return "";
	}
}
