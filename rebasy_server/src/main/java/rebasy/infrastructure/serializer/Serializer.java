package rebasy.infrastructure.serializer;

public interface Serializer {
	String keyValue(String key, Object value);

	String error(String message);

	String value(Object value);
}
