package rebasy.infrastructure.serializer;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectToJson implements Serializer {
	private Gson gson = new Gson();

	@Override
	public String keyValue(String key, Object value) {
		Map<String, Object> result = new HashMap<>();
		result.put(key, value);
		return gson.toJson(result);

	}

	@Override
	public String error(String message) {
		return keyValue("error", message);
	}

	@Override
	public String value(Object value) {
		return gson.toJson(value);
	}
}
