package rebasy.infrastructure;

import org.eclipse.jetty.http.HttpStatus;
import rebasy.infrastructure.api.*;
import rebasy.infrastructure.auth.AuthService;
import rebasy.infrastructure.serializer.ObjectToJson;
import rebasy.infrastructure.serializer.Serializer;
import rebasy.model.admins.AdminService;
import rebasy.model.customers.CustomerService;
import rebasy.model.orders.OrderService;
import rebasy.model.orders.ProductService;
import spark.ExceptionHandler;

import java.util.function.Function;

import static spark.Spark.*;

public class RebasyServer {

	private static final String API_PREFIX = "/api";
	private static final String API_VERSION = "1.0";

	private final AdminService adminService;
	private final CustomerService customerService;
	private final OrderService orderService;
	private final AuthService authService;
	private final int port;
	private final Serializer serializer;

	public RebasyServer(AdminService adminService, CustomerService customerService, OrderService orderService, AuthService authService, int port) {
		this.adminService = adminService;
		this.customerService = customerService;
		this.orderService = orderService;
		this.authService = authService;
		this.port = port;
		this.serializer = new ObjectToJson();
	}

	public void start() {
		port(port);
		before(endpoint("/*"), new PermissionChecker(authService));
		addAuthenticator();
		addBaseRoutes();
		addAdminRoutes();
		addCustomerRoutes();
		addOrderRoutes();
		addExceptionHandlers();
	}

	private void addAuthenticator() {
		Login login = new Login(authService, serializer);
		get("/login", login);
		get("/oauth2callback", new OAuth2Callback(authService, adminService, serializer));

	}

	private void addBaseRoutes() {
		get("/", (req, res) -> "Welcome to Record Backend System");
	}

	private void addAdminRoutes() {
		post(endpoint("/admin"), new PostAdmin(adminService, serializer));
		delete(endpoint("/admin"), new DeleteAdmin(adminService, serializer));
	}

	private void addCustomerRoutes() {
		post(endpoint("/customer"), new PostCustomer(customerService, serializer));
		get(endpoint("/customer"), new GetCustomers(customerService, serializer));
		get(endpoint("/customer/:") + Parameters.CUSTOMER_ID, new GetCustomer(customerService, serializer));
		delete(endpoint("/customer/:") + Parameters.CUSTOMER_ID, new DeleteCustomer(customerService, serializer));
	}

	private void addOrderRoutes() {
		get(endpoint("/order/customer/:") + Parameters.CUSTOMER_ID, new GetOrdersOfCustomer(orderService, serializer));
		post(endpoint("/order"), new PostOrder(orderService, serializer));
		get(endpoint("/order/:") + Parameters.ORDER_ID, new GetOrder(orderService, serializer));
	}

	private String endpoint(String endPoint) {
		return API_PREFIX + "/" + API_VERSION + endPoint;
	}

	private void addExceptionHandlers() {
		Function<Integer, ExceptionHandler> handler =
				code -> (exception, request, response) -> {
					response.status(code);
					response.body(errorMessage(exception));
				};

		exception(AdminService.AdminAlreadyExist.class,
				handler.apply(HttpStatus.BAD_REQUEST_400));

		exception(CustomerService.CustomerAlreadyExistException.class,
				handler.apply(HttpStatus.BAD_REQUEST_400));

		exception(PermissionChecker.PermissionDenied.class,
				handler.apply(HttpStatus.FORBIDDEN_403));

		exception(Helpers.MissingParameterException.class,
				handler.apply(HttpStatus.BAD_REQUEST_400));

		exception(AuthService.InvalidUser.class,
				handler.apply(HttpStatus.FORBIDDEN_403));

		exception(Helpers.EmptyBody.class,
				handler.apply(HttpStatus.BAD_REQUEST_400));

		exception(CustomerService.CustomerNotFound.class,
				handler.apply(HttpStatus.NOT_FOUND_404));

		exception(ProductService.ProductNotFound.class,
				handler.apply(HttpStatus.NOT_FOUND_404));

		exception(OrderService.OrderNotFound.class,
				handler.apply(HttpStatus.NOT_FOUND_404));
	}

	private String errorMessage(Exception exception) {
		return serializer.error(exception.getClass().getSimpleName() + " " + exception.getMessage());
	}
}
