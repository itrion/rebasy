package rebasy.infrastructure.repositories;

import rebasy.model.orders.Product;
import rebasy.model.orders.ProductID;
import rebasy.model.orders.ProductRepository;

import java.util.*;

public class InMemoryProductRepository implements ProductRepository {
	private Map<ProductID, Product> products = new HashMap<>();

	public InMemoryProductRepository() {
		newProduct("product 1", "foo");
		newProduct("product 2", "bar");
		newProduct("product 3", "foobar");
	}

	@Override
	public Product newProduct(String name, String description) {
		ProductID productID = new ProductID(UUID.randomUUID().toString());
		Product product = new Product(productID, name, description);
		products.put(productID, product);
		return product;
	}

	@Override
	public boolean exist(ProductID productId) {
		return products.containsKey(productId);
	}

	@Override
	public Product getProduct(ProductID productId) {
		return products.get(productId);
	}

	@Override
	public List<ProductID> getProductsIds() {
		return new ArrayList<>(products.keySet());
	}
}
