package rebasy.infrastructure.repositories;

import rebasy.model.customers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class InMemoryCustomerRepository implements CustomerRepository {

	private Map<String, Customer> customers = new HashMap<>();

	public InMemoryCustomerRepository() {
		newCustomer("Customer", "1", new DNI("1"), new Photo("any"));
		newCustomer("Customer", "2", new DNI("2"), new Photo("any"));
		newCustomer("Customer", "3", new DNI("3"), new Photo("any"));
	}

	@Override
	public void delete(CustomerID id) {
		customers.remove(id.value());
	}

	@Override
	public Customer newCustomer(String name, String lastName, DNI dni, Photo photo) {
		Customer customer = new Customer(name, lastName, dni, photo);
		customers.put(customer.id().value(), customer);
		return customer;
	}

	@Override
	public boolean exist(CustomerID id) {
		return customers.containsKey(id.value());
	}

	@Override
	public List<CustomerID> customerIds() {
		return customers.keySet().stream()
				.map(DNI::new)
				.collect(toList());
	}

	@Override
	public Customer getCustomer(CustomerID id) {
		return customers.get(id.value());
	}
}
