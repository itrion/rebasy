package rebasy.infrastructure.repositories;

import rebasy.model.customers.CustomerID;
import rebasy.model.orders.Date;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderRepository;
import rebasy.model.orders.ProductID;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

public class InMemoryOrderRepository implements OrderRepository {
	private Map<OrderID, OrderRecord> orders = new HashMap<>();

	@Override
	public OrderID newOrder(Date date, CustomerID customerID, ProductID productID) {
		OrderID id = new OrderID(UUID.randomUUID().toString());
		orders.put(id, new OrderRecord(id, date, customerID, productID));
		return id;
	}

	@Override
	public boolean exist(OrderID orderId) {
		return orders.containsKey(orderId);
	}

	@Override
	public void delete(OrderID orderId) {
		orders.remove(orderId);
	}

	@Override
	public List<OrderRecord> getOrders() {
		return orders.values().stream().collect(toList());
	}

	@Override
	public OrderRecord getOrderRecord(OrderID orderID) {
		return orders.get(orderID);
	}
}
