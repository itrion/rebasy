package rebasy.infrastructure.repositories;

import rebasy.model.admins.AdminRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryAdminRepository implements AdminRepository {
	private List<String> admins = new ArrayList<>();

	public InMemoryAdminRepository() {
		admins.add("recordbackendsystem@gmail.com");
	}

	@Override
	public void createNewAdmin(String userName) {
		admins.add(userName);
	}

	@Override
	public boolean exist(String userName) {
		return admins.contains(userName);
	}

	@Override
	public void deleteAdmin(String userName) {
		admins.remove(userName);
	}
}
