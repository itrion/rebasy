package rebasy.model.admins;

public interface AdminRepository {
	void createNewAdmin(String userName);

	boolean exist(String userName);

	void deleteAdmin(String userName);
}
