package rebasy.model.admins;

public class AdminService {
	private final AdminRepository adminRepository;

	public AdminService(AdminRepository adminRepository) {
		this.adminRepository = adminRepository;
	}

	public void createNewAdmin(String adminName) throws AdminAlreadyExist {
		if (exist(adminName)) throw new AdminAlreadyExist(adminName);
		adminRepository.createNewAdmin(adminName);
	}

	public boolean exist(String adminName) {
		return adminRepository.exist(adminName);
	}

	public void deleteAdmin(String adminName) {
		if (exist(adminName)) adminRepository.deleteAdmin(adminName);
	}

	public static class AdminAlreadyExist extends Exception {
		public AdminAlreadyExist(String adminName) {
			super(adminName);
		}
	}

}
