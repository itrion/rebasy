package rebasy.model.customers;

public class Photo {
	private final String base64;

	public Photo(String base64) {
		this.base64 = base64;
	}

	public String base64() {
		return base64;
	}
}
