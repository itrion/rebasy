package rebasy.model.customers;

import java.util.List;

public class CustomerService {

	private final CustomerRepository customerRepository;

	public CustomerService(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public boolean exist(CustomerID id) {
		return customerRepository.exist(id);
	}

	public Customer addCustomer(String name, String lastName, DNI dni, Photo photo) throws CustomerAlreadyExistException {
		if (exist(dni)) throw new CustomerAlreadyExistException(dni);
		return customerRepository.newCustomer(name, lastName, dni, photo);
	}

	public void delete(CustomerID id) {
		if (exist(id))
			customerRepository.delete(id);
	}

	public List<CustomerID> getCustomersIds() {
		return customerRepository.customerIds();
	}

	public Customer getCustomerWith(CustomerID id) throws CustomerNotFound {
		if (!exist(id)) throw new CustomerNotFound(id);
		return customerRepository.getCustomer(id);
	}

	public static class CustomerAlreadyExistException extends Exception {
		public CustomerAlreadyExistException(CustomerID customer) {
			super(customer.value());
		}
	}

	public static class CustomerNotFound extends Exception {
		public CustomerNotFound(CustomerID id) {
			super(id.value());
		}
	}

}
