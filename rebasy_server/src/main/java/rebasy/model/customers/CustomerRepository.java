package rebasy.model.customers;

import java.util.List;

public interface CustomerRepository {
	void delete(CustomerID id);

	Customer newCustomer(String name, String lastName, DNI dni, Photo photo);

	boolean exist(CustomerID id);

	List<CustomerID> customerIds();

	Customer getCustomer(CustomerID id);
}
