package rebasy.model.customers;

public class Customer {
	private final DNI dni;
	private final String name;
	private final String lastName;
	private final Photo photo;

	public Customer(String name, String lastName, DNI dni, Photo photo) {
		this.name = name;
		this.lastName = lastName;
		this.dni = dni;
		this.photo = photo;
	}

	public String name() {
		return name;
	}

	public String lastName() {
		return lastName;
	}

	public CustomerID id() {
		return dni;
	}

	public Photo photo() {
		return photo;
	}
}
