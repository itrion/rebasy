package rebasy.model.orders;

import java.util.List;

public interface ProductRepository {

	Product newProduct(String name, String description);

	boolean exist(ProductID productId);

	Product getProduct(ProductID productId);

	List<ProductID> getProductsIds();
}
