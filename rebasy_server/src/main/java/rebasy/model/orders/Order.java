package rebasy.model.orders;

import rebasy.model.customers.Customer;

public class Order {
	private final OrderID id;
	private final Date date;
	private final Customer customer;
	private final Product product;

	public Order(OrderID id, Date date, Customer customer, Product product) {
		this.id = id;
		this.date = date;
		this.customer = customer;
		this.product = product;
	}

	public OrderID id() {
		return id;
	}

	public Date date() {
		return date;
	}

	public Customer customer() {
		return customer;
	}

	public Product product() {
		return product;
	}
}
