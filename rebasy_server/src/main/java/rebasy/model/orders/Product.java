package rebasy.model.orders;

public class Product {
	private final ProductID id;
	private final String name;
	private final String description;

	public Product(ProductID id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public ProductID id() {
		return id;
	}

	public String name() {
		return name;
	}

	public String description() {
		return description;
	}
}
