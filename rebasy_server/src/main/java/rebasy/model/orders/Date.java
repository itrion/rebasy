package rebasy.model.orders;

public class Date {
	private final String date;

	public Date(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return date;
	}
}
