package rebasy.model.orders;

import rebasy.model.customers.CustomerID;

import java.util.List;

public interface OrderRepository {
	OrderID newOrder(Date date, CustomerID customerID, ProductID productID);

	boolean exist(OrderID orderId);

	void delete(OrderID orderId);

	List<OrderRecord> getOrders();

	OrderRecord getOrderRecord(OrderID orderID);

	class OrderRecord {
		final OrderID id;
		final Date date;
		final CustomerID customerID;
		final ProductID productID;

		public OrderRecord(OrderID id, Date date, CustomerID customerID, ProductID productID) {
			this.id = id;
			this.date = date;
			this.customerID = customerID;
			this.productID = productID;
		}
	}
}
