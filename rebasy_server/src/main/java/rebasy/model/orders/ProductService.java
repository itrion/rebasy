package rebasy.model.orders;

import java.util.List;
import java.util.stream.Collectors;

public class ProductService {
	private final ProductRepository productRepository;

	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public Product getProduct(ProductID productID) throws ProductNotFound {
		if (!exist(productID)) throw new ProductNotFound(productID);
		return productRepository.getProduct(productID);
	}

	private boolean exist(ProductID productID) {
		return productRepository.exist(productID);
	}

	public List<Product> getProducts() {
		return productRepository.getProductsIds().stream()
				.map(productRepository::getProduct)
				.collect(Collectors.toList());
	}

	public static class ProductNotFound extends Exception {
		public ProductNotFound(ProductID productID) {
			super(productID.value());
		}
	}
}
