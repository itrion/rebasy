package rebasy.model.orders;

import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.CustomerService.CustomerNotFound;
import rebasy.model.orders.OrderRepository.OrderRecord;
import rebasy.model.orders.ProductService.ProductNotFound;

import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class OrderService {
	private final OrderRepository orderRepository;
	private final CustomerService customerService;
	private final ProductService productService;

	public OrderService(OrderRepository orderRepository, CustomerService customerService, ProductService productService) {
		this.orderRepository = orderRepository;
		this.customerService = customerService;
		this.productService = productService;
	}

	public Order createOrder(Date date, CustomerID customerID, ProductID productID) throws CustomerNotFound, ProductNotFound {
		Customer customer = customerService.getCustomerWith(customerID);
		Product product = productService.getProduct(productID);
		return new Order(orderRepository.newOrder(date, customerID, productID), date, customer, product);
	}

	public boolean exist(OrderID orderId) {
		return orderRepository.exist(orderId);
	}

	public void deleteOrder(OrderID orderId) {
		if (exist(orderId)) orderRepository.delete(orderId);
	}

	public Order getOrder(OrderID orderID) throws OrderNotFound, ProductNotFound, CustomerNotFound {
		if (!exist(orderID)) throw new OrderNotFound(orderID);
		return assembleOrderFrom(orderRepository.getOrderRecord(orderID));
	}

	private Order assembleOrderFrom(OrderRecord orderRecord) throws CustomerNotFound, ProductNotFound {
		return new Order(orderRecord.id,
				orderRecord.date,
				customerService.getCustomerWith(orderRecord.customerID),
				productService.getProduct(orderRecord.productID));
	}

	public List<OrderID> getOrdersBy(CustomerID customerID) {
		return orderRepository.getOrders().stream()
				.filter(by(customerID))
				.map(it -> it.id)
				.collect(toList());
	}

	private Predicate<OrderRecord> by(CustomerID criteria) {
		return record -> criteria.equals(record.customerID);
	}

	public static class OrderNotFound extends Exception {
		public OrderNotFound(OrderID orderID) {
			super(orderID.value());
		}
	}
}
