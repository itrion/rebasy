package rebasy.model;

public class ID {
	protected final String id;

	public ID(String value) {
		this.id = value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ID)) return false;

		ID id1 = (ID) o;

		return !(id != null ? !id.equals(id1.id) : id1.id != null);

	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	public String value() {
		return id;
	}
}
