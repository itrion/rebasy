package rebasy.infrastructure.serializer.google;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PersonResponse_ {
	@Test
	public void should_extract_the_email_from_a_json_containing_an_emails_list() throws Exception {
		String response = "{\"emails\": [\n" +
				"    {\n" +
				"      \"type\": \"account\", \n" +
				"      \"value\": \"any@email.com\"\n" +
				"    }\n" +
				"  ]}";
		assertThat(new PersonResponseParser(response).email(), is("any@email.com"));
	}
}
