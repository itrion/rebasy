package rebasy.usecases.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetOrder_ {
	@Mock OrderID anyOrderId;
	@Mock OrderService anyOrderService;

	@Test
	public void should_call_get_order_in_the_order_service_with_the_desired_order_id() throws Exception {
		new GetOrder(anyOrderService).execute(anyOrderId);

		verify(anyOrderService, times(1)).getOrder(anyOrderId);
	}
}
