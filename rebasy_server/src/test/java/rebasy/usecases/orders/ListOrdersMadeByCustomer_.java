package rebasy.usecases.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.CustomerID;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ListOrdersMadeByCustomer_ {

	@Mock OrderService anyOrderService;
	@Mock CustomerID anyCustomerId;

	@Test
	public void should_call_get_orders_by_customer_id_in_order_service() throws Exception {
		OrderID anyOrder = mock(OrderID.class);
		when(anyOrderService.getOrdersBy(anyCustomerId)).thenReturn(singletonList(anyOrder));

		new ListOrdersMadeByCustomer(anyOrderService).execute(anyCustomerId);

		verify(anyOrderService, times(1)).getOrdersBy(anyCustomerId);
	}
}
