package rebasy.usecases.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.CustomerID;
import rebasy.model.orders.Date;
import rebasy.model.orders.Order;
import rebasy.model.orders.OrderService;
import rebasy.model.orders.ProductID;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PlaceOrder_ {

	@Mock OrderService anyOrderService;
	@Mock Date anyDate;
	@Mock CustomerID anyCustomerId;
	@Mock ProductID anyProductId;

	@Test
	public void should_call_add_order_in_the_order_service_with_a_date_a_customer_id_and_a_product_id() throws Exception {
		Order order = new PlaceOrder(anyOrderService).execute(anyDate, anyCustomerId, anyProductId);

		verify(anyOrderService, times(1)).createOrder(anyDate, anyCustomerId, anyProductId);
	}
}
