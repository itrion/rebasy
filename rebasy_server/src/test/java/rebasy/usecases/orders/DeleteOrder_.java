package rebasy.usecases.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.orders.OrderID;
import rebasy.model.orders.OrderService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DeleteOrder_ {

	@Mock OrderService anyOrderService;
	@Mock OrderID anyOrderId;

	@Test
	public void should_call_delete_with_an_order_id_in_the_order_service() throws Exception {
		new DeleteOrder(anyOrderService).execute(anyOrderId);

		verify(anyOrderService, times(1)).deleteOrder(anyOrderId);
	}
}
