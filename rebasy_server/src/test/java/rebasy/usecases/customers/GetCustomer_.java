package rebasy.usecases.customers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetCustomer_ {

	@Mock CustomerService anyCustomerService;
	@Mock DNI anyCustomerID;

	@Test
	public void should_call_get_customer_with_in_the_customer_service() throws CustomerService.CustomerNotFound {
		Customer ignoreResult = new GetCustomer(anyCustomerService).execute(anyCustomerID);

		verify(anyCustomerService, times(1)).getCustomerWith(anyCustomerID);
	}
}
