package rebasy.usecases.customers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;
import rebasy.model.customers.Photo;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddCustomer_ {
	@Mock CustomerService customerService;
	@Mock DNI anyID;
	@Mock Photo anyPhoto;
	String anyName;
	String anyLastName;
	Customer anyCustomer;

	@Before
	public void setUp() throws Exception {
		anyName = "Any Name";
		anyLastName = "Any Last Name";
		anyCustomer = new Customer(anyName, anyLastName, anyID, anyPhoto);
	}

	@Test
	public void should_call_add_customer_in_customer_service() throws CustomerService.CustomerAlreadyExistException {
		when(customerService.exist(anyID)).thenReturn(false);

		new AddCustomer(customerService).execute(anyName, anyLastName, anyID, anyPhoto);

		verify(customerService, times(1)).addCustomer(anyName, anyLastName, anyID, anyPhoto);
	}
}
