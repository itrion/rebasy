package rebasy.usecases.customers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteCustomer_ {
	@Mock CustomerService customerService;
	@Mock DNI anyID;

	@Test
	public void should_call_delete_in_customer_service() {
		new DeleteCustomer(customerService).execute(anyID);

		verify(customerService, times(1)).delete(anyID);
	}
}
