package rebasy.usecases.customers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.CustomerID;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.DNI;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetCustomers_ {

	@Mock CustomerService customerService;

	@Test
	public void should_call_get_customers_ids_in_customer_service() {
		List<CustomerID> ignoreResult = new GetCustomers(customerService).execute();

		verify(customerService, times(1)).getCustomersIds();
	}
}
