package rebasy.usecases.admins;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.admins.AdminService;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RemoveAdminUser_ {

	@Mock AdminService adminService;
	String anyName = "anyName";

	@Test
	public void should_call_delete_admin_in_the_admin_service() {
		when(adminService.exist(anyName)).thenReturn(true);

		new RemoveAdminUser(adminService).execute(anyName);

		verify(adminService, times(1)).deleteAdmin(anyName);
	}
}
