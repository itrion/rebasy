package rebasy.usecases.admins;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.admins.AdminService;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RegisterAdminUser_ {

	@Mock AdminService adminService;
	String anyUserName = "any";

	@Test
	public void should_call_create_admin_in_the_admin_service() throws AdminService.AdminAlreadyExist {
		new RegisterAdminUser(adminService).execute(anyUserName);

		verify(adminService).createNewAdmin(anyUserName);
	}
}
