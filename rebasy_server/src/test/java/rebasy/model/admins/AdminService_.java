package rebasy.model.admins;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static rebasy.model.admins.AdminService.AdminAlreadyExist;

@RunWith(MockitoJUnitRunner.class)
public class AdminService_ {

	@Mock AdminRepository anyAdminRepository;
	AdminService adminService;
	String anyAdminName;

	@Before
	public void setUp() throws Exception {
		adminService = new AdminService(anyAdminRepository);
		anyAdminName = "any";
	}

	@Test
	public void should_create_an_admin_in_the_repository_when_admin_does_not_exist() throws AdminAlreadyExist {

		adminService.createNewAdmin(anyAdminName);

		verify(anyAdminRepository).createNewAdmin(anyAdminName);
	}

	@Test
	public void should_throw_an_exception_when_admin_already_exist_in_the_repository() {
		String anyUserName = "anyUserName";
		try {
			when(anyAdminRepository.exist(anyUserName)).thenReturn(true);

			adminService.createNewAdmin(anyUserName);
			fail();
		} catch (AdminAlreadyExist e) {
			verify(anyAdminRepository, times(1)).exist(anyUserName);
		}
	}

	@Test
	public void should_remove_the_given_user_from_the_repository_when_the_user_already_exist() {
		when(anyAdminRepository.exist(anyAdminName)).thenReturn(true);

		adminService.deleteAdmin(anyAdminName);

		verify(anyAdminRepository, times(1)).exist(anyAdminName);
		verify(anyAdminRepository, times(1)).deleteAdmin(anyAdminName);
	}

	@Test
	public void should_do_nothing_when_user_does_not_exist_in_the_repository() {
		when(anyAdminRepository.exist(anyAdminName)).thenReturn(false);

		adminService.deleteAdmin(anyAdminName);

		verify(anyAdminRepository, times(1)).exist(anyAdminName);
		verifyNoMoreInteractions(anyAdminRepository);
	}

}
