package rebasy.model.orders;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.Customer;
import rebasy.model.customers.CustomerService;
import rebasy.model.customers.CustomerService.CustomerNotFound;
import rebasy.model.customers.DNI;
import rebasy.model.orders.OrderService.OrderNotFound;
import rebasy.model.orders.ProductService.ProductNotFound;

import static junit.framework.TestCase.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static rebasy.model.orders.OrderRepository.OrderRecord;

@RunWith(MockitoJUnitRunner.class)
public class OrderService_ {

	@Mock OrderRepository anyOrderRepository;
	@Mock Order anyOrder;
	@Mock Date anyDate;
	@Mock Customer anyCustomer;
	@Mock Product anyProduct;
	@Mock CustomerService anyCustomerService;
	@Mock ProductService anyProductService;
	OrderService orderService;

	@Before
	public void setUp() throws Exception {
		orderService = new OrderService(anyOrderRepository, anyCustomerService, anyProductService);
	}

	@After
	public void cleanUp() {
	}

	@Test
	public void should_create_an_order_in_the_order_repository_when_create_order_is_called() throws Exception {
		orderService.createOrder(anyDate, anyCustomer.id(), anyProduct.id());

		verify(anyOrderRepository, times(1)).newOrder(anyDate, anyCustomer.id(), anyProduct.id());
		verify(anyCustomerService, times(1)).getCustomerWith(anyCustomer.id());
		verify(anyProductService, times(1)).getProduct(anyProduct.id());
	}

	@Test
	public void should_return_an_order_with_an_order_id_when_create_order_is_called() throws Exception {
		OrderID anyOrderId = mock(OrderID.class);
		when(anyOrderRepository.newOrder(anyDate, anyCustomer.id(), anyProduct.id())).thenReturn(anyOrderId);

		Order order = orderService.createOrder(anyDate, anyCustomer.id(), anyProduct.id());

		assertThat(order.id(), is(anyOrderId));
	}


	@Test
	public void should_throw_product_not_found_when_adding_and_order_for_product_which_does_not_exist_in_product_repository() throws Exception {
		try {
			when(anyProductService.getProduct(anyProduct.id())).thenThrow(ProductNotFound.class);
			orderService.createOrder(anyDate, anyCustomer.id(), anyProduct.id());
			fail();
		} catch (ProductNotFound | CustomerNotFound ignored) {
		}
	}

	@Test
	public void should_throw_customer_not_found_when_adding_an_order_for_customer_which_does_not_exist() throws Exception {
		try {
			when(anyCustomerService.getCustomerWith(anyCustomer.id())).thenThrow(new CustomerNotFound(new DNI("")));

			orderService.createOrder(anyDate, anyCustomer.id(), anyProduct.id());
			fail();
		} catch (CustomerNotFound ignored) {
		}
	}

	@Test
	public void should_delete_an_order_from_the_repository_when_delete_is_called_and_order_exist() throws Exception {
		OrderID anyOrderId = mock(OrderID.class);
		when(anyOrderRepository.exist(anyOrderId)).thenReturn(true);

		orderService.deleteOrder(anyOrderId);

		verify(anyOrderRepository, times(1)).exist(anyOrderId);
		verify(anyOrderRepository, times(1)).delete(anyOrderId);
		verifyNoMoreInteractions(anyOrderRepository);
	}

	@Test
	public void should_not_call_delete_in_the_repository_when_delete_is_called_and_order_does_not_exist() throws Exception {
		OrderID anyOrderId = mock(OrderID.class);
		when(anyOrderRepository.exist(anyOrderId)).thenReturn(false);

		orderService.deleteOrder(anyOrderId);

		verify(anyOrderRepository, times(1)).exist(anyOrderId);
		verifyNoMoreInteractions(anyOrderRepository);
	}

	@Test
	public void should_throw_order_not_found_exception_when_order_does_not_exist_in_the_repository() throws ProductNotFound, CustomerNotFound {
		OrderID anyOrderId = mock(OrderID.class);
		try {
			when(anyOrderRepository.exist(anyOrderId)).thenReturn(false);
			orderService.getOrder(anyOrderId);

			fail();
		} catch (OrderNotFound orderNotFound) {
			verify(anyOrderRepository, times(1)).exist(anyOrderId);
		}
	}

	@Test
	public void should_get_the_orders_from_the_order_repository() {
		orderService.getOrdersBy(anyCustomer.id());
		verify(anyOrderRepository, times(1)).getOrders();
	}

	@Test
	public void should_build_an_order_using_the_customer_and_product_service() throws Exception {
		OrderID anyOrderId = mock(OrderID.class);
		OrderRecord anyOrderRecord = mock(OrderRecord.class);
		when(anyOrderRepository.exist(anyOrderId)).thenReturn(true);
		when(anyOrderRepository.getOrderRecord(anyOrderId)).thenReturn(anyOrderRecord);

		orderService.getOrder(anyOrderId);

		verify(anyCustomerService, times(1)).getCustomerWith(any());
		verify(anyProductService, times(1)).getProduct(any());
	}
}
