package rebasy.model.orders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductService_ {

	@Mock ProductRepository anyProductRepository;
	@Mock ProductID anyProductID;
	ProductService productService;

	@Before
	public void setUp() throws Exception {
		productService = new ProductService(anyProductRepository);
	}

	@Test
	public void should_retrieve_a_product_from_the_product_repository_when_the_product_exists() throws Exception {
		when(anyProductRepository.exist(anyProductID)).thenReturn(true);

		productService.getProduct(anyProductID);

		verify(anyProductRepository, times(1)).exist(anyProductID);
		verify(anyProductRepository, times(1)).getProduct(anyProductID);
	}

	@Test
	public void should_throw_an_exception_when_the_requested_product_id_is_not_in_the_repository() throws Exception {
		try {
			when(anyProductRepository.exist(anyProductID)).thenReturn(false);

			productService.getProduct(anyProductID);
			fail();
		} catch (ProductService.ProductNotFound productNotFound) {
			verify(anyProductRepository, times(1)).exist(anyProductID);
			verifyNoMoreInteractions(anyProductRepository);
		}
	}

	@Test
	public void should_provide_all_the_products_in_the_repository() throws Exception {
		Product anyProduct = mock(Product.class);
		when(anyProductRepository.getProductsIds()).thenReturn(singletonList(anyProductID));
		when(anyProductRepository.getProduct(anyProductID)).thenReturn(anyProduct);

		List<Product> ignoreResult = productService.getProducts();

		verify(anyProductRepository, times(1)).getProductsIds();
		verify(anyProductRepository, times(1)).getProduct(anyProductID);
	}
}
