package rebasy.model.customers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rebasy.model.customers.CustomerService.CustomerAlreadyExistException;
import rebasy.model.customers.CustomerService.CustomerNotFound;

import java.util.List;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerService_ {

	@Mock CustomerRepository customerRepository;
	@Mock DNI anyId;
	@Mock Photo anyPhoto;
	String anyName = "anyName";
	String anyLastName = "anyLastName";
	CustomerService service;

	@Before
	public void setUp() throws Exception {
		service = new CustomerService(customerRepository);
	}

	@After
	public void cleanUp() {
		verifyNoMoreInteractions(customerRepository);
	}

	@Test
	public void should_add_the_given_customer_to_the_repository_when_the_customer_does_not_exist() throws CustomerAlreadyExistException {
		when(customerRepository.exist(anyId)).thenReturn(false);

		service.addCustomer(anyName, anyLastName, anyId, anyPhoto);

		verify(customerRepository, times(1)).exist(anyId);
		verify(customerRepository, times(1)).newCustomer(anyName, anyLastName, anyId, anyPhoto);
	}

	@Test
	public void should_throw_an_exception_when_adding_an_existent_customer_to_the_repository() {
		try {
			when(customerRepository.exist(anyId)).thenReturn(true);

			service.addCustomer(anyName, anyLastName, anyId, anyPhoto);
			fail();
		} catch (CustomerAlreadyExistException e) {
			verify(customerRepository, times(1)).exist(anyId);
		}
	}

	@Test
	public void should_delete_customer_from_repository_when_customer_exists() {
		when(customerRepository.exist(anyId)).thenReturn(true);

		service.delete(anyId);

		verify(customerRepository, times(1)).exist(anyId);
		verify(customerRepository, times(1)).delete(anyId);
	}

	@Test
	public void should_not_delete_customer_from_repository_when_customer_does_not_exist() {
		when(customerRepository.exist(anyId)).thenReturn(false);

		service.delete(anyId);

		verify(customerRepository, times(1)).exist(anyId);
	}

	@Test
	public void should_call_exist_method_of_the_customer_repository_when_querying_customer_existence() {
		service.exist(anyId);

		verify(customerRepository, times(1)).exist(anyId);
	}

	@Test
	public void should_retrieve_the_customer_id_list_from_the_customer_repository() {
		List<CustomerID> usersIds = service.getCustomersIds();

		verify(customerRepository, times(1)).customerIds();
	}

	@Test
	public void should_retrieve_a_customer_by_his_id_from_the_customer_repository() throws CustomerNotFound {
		when(customerRepository.exist(anyId)).thenReturn(true);

		Customer ignoreResult = service.getCustomerWith(anyId);

		verify(customerRepository, times(1)).exist(anyId);
		verify(customerRepository, times(1)).getCustomer(anyId);
	}
	
	@Test
	public void should_throw_an_exception_when_getting_a_customer_who_does_not_exist() {
		try {
			when(customerRepository.exist(anyId)).thenReturn(false);

			service.getCustomerWith(anyId);
			fail();
		} catch (CustomerNotFound e) {
			verify(customerRepository, times(1)).exist(anyId);
		}
	}
}
